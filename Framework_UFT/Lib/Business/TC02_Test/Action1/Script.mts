﻿

ExitTest

'ExitAction − Exits the current action.
'
'ExitActionIteration − Exits the current iteration of the action.
'
'ExitTestIteration − Exits the current iteration of the UFT test and proceeds to the next iteration.
'


SystemUtil.Run "C:\Program Files (x86)\Micro Focus\Unified Functional Testing\samples\Flights Application\FlightsGUI.exe"

'------------ Import Data Excel------------'
'DataTable.Import "C:\Users\YingLee\OneDrive\UFT_Traning\TC02.xlsx"

Username = DataTable.Value("Username","TC02 [TC02_Test]")
Password = DataTable.Value("Password","TC02 [TC02_Test]")
Tickets = DataTable.Value("Tickets","TC02 [TC02_Test]")
Price = DataTable.Value("Price","TC02 [TC02_Test]")
passengerName = DataTable.Value("passengerName","TC02 [TC02_Test]")
'------------ Import Data Excel------------'

TransationStart "TC02_GUI_Login_01" 
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentNameee").Set Username @@ hightlight id_;_2130251128_;_script infofile_;_ZIP::ssf2.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("password").Set Password @@ hightlight id_;_2128590000_;_script infofile_;_ZIP::ssf3.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click @@ hightlight id_;_2130236584_;_script infofile_;_ZIP::ssf4.xml_;_
TransationEnd "TC02_GUI_Login_01" 

WpfWindow("Micro Focus MyFlight Sample").WpfComboBox("numOfTickets").Select Tickets @@ hightlight id_;_1963428456_;_script infofile_;_ZIP::ssf6.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("FIND FLIGHTS").Click @@ hightlight id_;_2130236920_;_script infofile_;_ZIP::ssf7.xml_;_
'-------Click FIND----------------------'

'-------------Count Row and Colum-----------'
Row = WpfWindow("Micro Focus MyFlight Sample").WpfTable("flightsDataGrid").RowCount
Colum = WpfWindow("Micro Focus MyFlight Sample").WpfTable("flightsDataGrid").ColumnCount
print Row
print Colum

For i = 0 To Row step 1
print "i="&i
	For j = 0 To (Colum -1)
	print "j="&j
		price = WpfWindow("Micro Focus MyFlight Sample").WpfTable("flightsDataGrid").GetCellData(i,j)
		print price
			
	If (price = Price) Then
	
	 Exit For 
	End If
   Next
   If (price = Price) Then
   	 WpfWindow("Micro Focus MyFlight Sample").WpfTable("flightsDataGrid").SelectCell i,j
   	 Exit for 
   End If
Next

'WpfWindow("Micro Focus MyFlight Sample").WpfTable("flightsDataGrid").SelectCell 4,1 @@ hightlight id_;_2128585296_;_script infofile_;_ZIP::ssf8.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("SELECT FLIGHT").Click @@ hightlight id_;_1957786432_;_script infofile_;_ZIP::ssf9.xml_;_


GetPrice = WpfWindow("Micro Focus MyFlight Sample").WpfObject("$149.00").GetROProperty("text")
GetPrice = mid(GetPrice,2,6)
GetPrice = CDBL(GetPrice*Tickets)
print GetPrice

GetPriceTotal = WpfWindow("Micro Focus MyFlight Sample").WpfObject("$248.40").GetROProperty("text")
GetPriceTotal = mid(GetPriceTotal,2,6)
GetPriceTotal = CDBL(GetPriceTotal)
print GetPriceTotal

If GetPrice =  GetPriceTotal Then
	WpfWindow("Micro Focus MyFlight Sample").WpfEdit("passengerName").Set passengerName
	else
	Msgbox Fail
End If
 @@ hightlight id_;_2130245272_;_script infofile_;_ZIP::ssf10.xml_;_
MyFlight = WpfWindow("Micro Focus MyFlight Sample").WpfObject("12510 AF").GetROProperty("text")
WpfWindow("Micro Focus MyFlight Sample").WpfButton("ORDER").Click @@ hightlight id_;_2130246088_;_script infofile_;_ZIP::ssf11.xml_;_

'Get Order number'
NumberOrder = WpfWindow("Micro Focus MyFlight Sample").WpfObject("Order 93 completed").GetROProperty("text") @@ hightlight id_;_1957262752_;_script infofile_;_ZIP::ssf14.xml_;_
NumberOrder = mid(NumberOrder,7,3)
print NumberOrder
'End Get Order number'


WpfWindow("Micro Focus MyFlight Sample").WpfButton("NEW SEARCH").Click @@ hightlight id_;_2130248440_;_script infofile_;_ZIP::ssf15.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfTabStrip("WpfTabStrip").Select "SEARCH ORDER" @@ hightlight id_;_2130249880_;_script infofile_;_ZIP::ssf16.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfRadioButton("byNumberRadio").Set @@ hightlight id_;_1950689760_;_script infofile_;_ZIP::ssf17.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("byNumberWatermark").Set NumberOrder @@ hightlight id_;_2130247624_;_script infofile_;_ZIP::ssf18.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("SEARCH").Click

MyFlight2 = WpfWindow("Micro Focus MyFlight Sample").WpfObject("12510 AF").GetROProperty("text")
If MyFlight = MyFlight2 Then
	WpfWindow("Micro Focus MyFlight Sample").WpfButton("WpfButton").Click
End If
 @@ hightlight id_;_1963422504_;_script infofile_;_ZIP::ssf21.xml_;_
WpfWindow("Micro Focus MyFlight Sample").Dialog("Notification").WinButton("Yes").Click @@ hightlight id_;_1117820_;_script infofile_;_ZIP::ssf22.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfTabStrip("WpfTabStrip").Select "SEARCH ORDER" @@ hightlight id_;_1963431288_;_script infofile_;_ZIP::ssf23.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfRadioButton("byNumberRadio").Set @@ hightlight id_;_2132269288_;_script infofile_;_ZIP::ssf24.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfEdit("byNumberWatermark").Set NumberOrder @@ hightlight id_;_1963425432_;_script infofile_;_ZIP::ssf27.xml_;_
 @@ hightlight id_;_2754356_;_script infofile_;_ZIP::ssf29.xml_;_
WpfWindow("Micro Focus MyFlight Sample").WpfButton("SEARCH").Click @@ hightlight id_;_1963426920_;_script infofile_;_ZIP::ssf28.xml_;_
WpfWindow("Micro Focus MyFlight Sample").Dialog("Error").WinButton("OK").Click @@ hightlight id_;_1770970_;_script infofile_;_ZIP::ssf30.xml_;_



'
'
'A = int("90")
'B = CINT("80")
'C = CINT("79")
'  
'Select Case iWeek = CINT("81")
'  Case iWeek >= A
'	    msgbox "A"
'  Case iWeek <= B
'	    msgbox "B"
'  Case iWeek <= C
'	    msgbox "C"
'    
'End Select


If WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").CheckProperty("Visible",True) Then
 Msgbox "True"
 Else 
 Msgbox "False"
End If
