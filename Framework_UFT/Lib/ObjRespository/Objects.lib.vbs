Option Explicit

'------------------------------------------------------------------------------------------------------------------'

Public sub app_Init(strWpfWindow)
   	On error resume next 
	obj_WpfWindow=strWpfWindow
	'obj_Page=strPage
'WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").Set
   If WpfWindow(obj_WpfWindow).Exist(0) Then    				   
		If err.number <>0 Then
			PrintInfo "micFail", "WpfWindow", " Error Description: " & Err.Description			  
			Err.clear						
			Exit sub
		End If
   	Else
	   PrintInfo "micFail", "WpfWindow", " Browser not found: " & strBrowser & " - " & strPage 	    
    End If             	
End sub

'------------------------------------------------------------------------------------------------------------------'

Public Sub StartTime(StrStartTime)

'StrStartTime = Time()
t = Timer
temp = Int(t)

Milliseconds = Int((t-temp) * 1000)

Seconds = temp mod 60
temp    = Int(temp/60)
Minutes = temp mod 60
Hours   = Int(temp/60)

'Print "Hours is "&" " & Hours &"| Minutes is "&" " & Minutes &"| Seconds is "&" " & Seconds &"| Milliseconds is "&" " & Milliseconds

StrStartTime = String(2 - Len(Hours), "0") & Hours & ":"
StrStartTime = StrStartTime & String(2 - Len(Minutes), "0") & Minutes & ":"
StrStartTime = StrStartTime & String(2 - Len(Seconds), "0") & Seconds & "."
StrStartTime = StrStartTime & String(4 - Len(Milliseconds), "0") & Milliseconds
Print StrStartTime
'MsgBox startTime
End Sub  

'------------------------------------------------------------------------------------------------------------------'

Public Sub EndTime(StrEndTime)

'StrEndTime = Time()
'MsgBox startTime

t = Timer
temp = Int(t)

Milliseconds = Int((t-temp) * 1000)

Seconds = temp mod 60
temp    = Int(temp/60)
Minutes = temp mod 60
Hours   = Int(temp/60)

'Print "Hours is "&" " & Hours &"| Minutes is "&" " & Minutes &"| Seconds is "&" " & Seconds &"| Milliseconds is "&" " & Milliseconds

StrEndTime = String(2 - Len(Hours), "0") & Hours & ":"
StrEndTime = StrEndTime & String(2 - Len(Minutes), "0") & Minutes & ":"
StrEndTime = StrEndTime & String(2 - Len(Seconds), "0") & Seconds & "."
StrEndTime = StrEndTime & String(4 - Len(Milliseconds), "0") & Milliseconds
'Print StrEndTime

End Sub  

'------------------------------------------------------------------------------------------------------------------'

Public Function Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)

	On error resume next 
	iRowCount = Datatable.getSheet("TestStep").getRowCount
 	Datatable.getSheet("TestStep").setCurrentRow(iRowCount+1)
 	DataTable.Value("No","TestStep") = strWebEdit
 	DataTable.Value("LogStep","TestStep") = StrStatus
 	DataTable.Value("CapScreen","TestStep") = Myfile
 	DataTable.Value("StartTime","TestStep") = StrStartTime
 	DataTable.Value("EndTime","TestStep") = StrEndTime
 	DataTable.Value("StepDescription","TestStep") = StepDescription 
 	DataTable.Value("Transation","TestStep") = Transation
 	
End Function  

'------------------------------------------------------------------------------------------------------------------'

Public Function GetparamiterTransactionStart(StrStartTime,Transation)

	On error resume next 
	iRowCount = Datatable.getSheet("LogTimeTransaction").getRowCount
 	Datatable.getSheet("LogTimeTransaction").setCurrentRow(iRowCount+1)
 	DataTable.Value("TransactionName","LogTimeTransaction") = Transation
 	DataTable.Value("StartTimeTransactionName","LogTimeTransaction") = StrStartTime
 	'DataTable.Value("EndTimeTransactionName","LogTimeTransaction") = StrEndTime
 	
 	
End Function  

'------------------------------------------------------------------------------------------------------------------'

Public Function GetparamiterTransactionEnd(StrEndTime,Transation)

	On error resume next 
	iRowCount = Datatable.getSheet("LogTimeTransaction").getRowCount
 	Datatable.getSheet("LogTimeTransaction").setCurrentRow(iRowCount)
 	DataTable.Value("TransactionName","LogTimeTransaction") = Transation
 	'DataTable.Value("StartTimeTransactionName","LogTimeTransaction") = StrStartTime
 	DataTable.Value("EndTimeTransactionName","LogTimeTransaction") = StrEndTime
 	
 	
End Function 

'------------------------------------------------------------------------------------------------------------------'

Public Sub MRPressKey(actionName,obj_WpfWindow,obj_frame,strWebEdit,index)

	On error resume next 
	Call StartTime(StrStartTime)
	If index <> "" Then
		With JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame)		
   			If .JavaTable(strWebEdit).Exist(60) Then
				JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).Click 321,132,"LEFT"
			    JavaWindow(obj_WpfWindow).JavaInternalFrame(obj_frame).JavaTable(strWebEdit).PressKey index ,micCtrl

  				Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", strWebEdit, strWebEdit & "WebEdit not found."   
   			    StrStatus = strWebEdit & "Step is False"
   			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,Transation) 	
				'Call Killservicejava
				call Rerun(actionName,obj_WpfWindow)
         	End If         	
		End With			
	End If
End Sub 

'------------------------------------------------------------------------------------------------------------------'

Function MSendKeys(key)
	Set Obj = CreateObject("Wscript.Shell")
	Obj.SendKeys key,1
End Function

'------------------------------------------------------------------------------------------------------------------'

Public Function General_CloseProcess(strProgramName)
   Dim objshell
   Set objshell=CreateObject("WScript.Shell")
   objshell.Run "TASKKILL /F /IM "& strProgramName
   Set objshell=nothing
End Function

'------------------------------------------------------------------------------------------------------------------'

Public Function WebLinkClick(obj_WpfWindow,strpage,strWebEdit)

	On error resume next 
	Call StartTime(StrStartTime)
	If strWebEdit <> "" Then			
			   'Browser("Automated Testing Shop").Page("Automated Testing Shop").Link("Sign in").Click
   			If  Browser(obj_WpfWindow).Page(strpage).Link(strWebEdit).Exist(20) Then
   			    Browser(obj_WpfWindow).Page(strpage).Link(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
				Browser(obj_WpfWindow).Page(strpage).Link(strWebEdit).Click
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Click" &" "& strWebEdit
        		print StepDescription
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
 
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'

Public Function WebEditSet(obj_WpfWindow,strpage,strWebEdit,strValue)
	
	On error resume next 
	Call StartTime(StrStartTime)
	If strWebEdit <> "" Then			
			   'Browser("Automated Testing Shop").Page("Automated Testing Shop").WebEdit("email").Set
   			If  Browser(obj_WpfWindow).Page(strpage).WebEdit(strWebEdit).Exist(20) Then
   			    Browser(obj_WpfWindow).Page(strpage).WebEdit(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
				Browser(obj_WpfWindow).Page(strpage).WebEdit(strWebEdit).Set strValue
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Input" &" "& strWebEdit &" "& strValue
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'
Public Function WebEditCheck(obj_WpfWindow,strpage,strWebEdit)
	
	On error resume next 
	Call StartTime(StrStartTime)
	If strWebEdit <> "" Then			
			   'Browser("Automated Testing Shop").Page("Automated Testing Shop").WebEdit("email").Set
   			If  Browser(obj_WpfWindow).Page(strpage).WebEdit(strWebEdit).Exist(20) Then
   			    'Browser(obj_WpfWindow).Page(strpage).WebEdit(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Check Object Page" &" "& strWebEdit 
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'

Public Function WebButtonClick(obj_WpfWindow,strpage,strWebEdit)
	
	On error resume next 
	Call StartTime(StrStartTime)
	If strWebEdit <> "" Then			
			   'Browser("Login").Page("Login").WebButton("Login").Click
   			If  Browser(obj_WpfWindow).Page(strpage).WebButton(strWebEdit).Exist(20) Then
   			    Browser(obj_WpfWindow).Page(strpage).WebButton(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
				Browser(obj_WpfWindow).Page(strpage).WebButton(strWebEdit).Click
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Click Button" &" "& strWebEdit 
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'

Public Function WebImageClick(obj_WpfWindow,strpage,strWebEdit)
	
	On error resume next 
	Call StartTime(StrStartTime)
	If strWebEdit <> "" Then			
			   'Browser("My account").Page("My account").Image("My Store").Click
   			If  Browser(obj_WpfWindow).Page(strpage).Image(strWebEdit).Exist(20) Then
   			    Browser(obj_WpfWindow).Page(strpage).Image(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
				Browser(obj_WpfWindow).Page(strpage).Image(strWebEdit).Click
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Click Image" &" "& strWebEdit 
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'

Public Function WebForLoop(obj_WpfWindow,strpage,strWebEdit,value)
	
	On error resume next 
	Call StartTime(StrStartTime)

	If strWebEdit <> "" Then			
			   'Browser("My account").Page("Hummingbird printed t-shirt").WebElement("Add T-shirt").Click
   			If  Browser(obj_WpfWindow).Page(strpage).WebElement(strWebEdit).Exist(20) Then
   			    Browser(obj_WpfWindow).Page(strpage).WebElement(strWebEdit).highlight	
   			    Call CaptureScreenShot(strFileName)	
   			    For Iterator = 1 To value
   			    Svalue = Iterator +1
				Browser(obj_WpfWindow).Page(strpage).WebElement(strWebEdit).Click
				Next
				Call EndTime(StrEndTime)	
				PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        		StrStatus = strWebEdit &" "& "Step is Pass"
        		StepDescription = "Click" &" "& strWebEdit &" "& Svalue
        		Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)
   			Else
				strResultStatus = "fail"
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime,StepDescription,Transation)				
         	End If         					
	End If
End function 

'------------------------------------------------------------------------------------------------------------------'


Public Function ExportData(title,sheet,value)
	DataTable.Value(title,sheet) = value
'	DataTable.Value("Name","TC01_Demo [TC01_Demo]") = Name
'	DataTable.Value("Price","TC01_Demo [TC01_Demo]") = Price
'	DataTable.Value("discount","TC01_Demo [TC01_Demo]") = discount

End Function

Public Function pathExport(path)
	DataTable.Export path
End Function


'Public Function WebForLoop(valuefor,valueto)
'For i = valuefor To valueto
'	Browser("Login").Page("Login").WebButton("WebButton").Click
'Next
'End Function


Public Function WebmidTotal(Total)
	Total = mid(Total,2)
    If Sum = Total Then
		Print Pass
	Else 
		Print Fail
	End If
End Function

Public Function WebmidPrie(Prie)

 Prie = mid(Prie,2)
 Prie = CDBL(Prie*i) 'ราคาสินค้า*จำนวน
 Prie1 = CDBL(Prie*20/100)  'ราคาเต็ม*ส่วนลด/100
 Sum = CDBL(Prie-Prie1)
 print Sum
End Function












